# RPI Video Player #

```
optional arguments:
  -h, --help            show this help message and exit
  -f MEDIA_FOLDER, --media_folder MEDIA_FOLDER
                        Folder containing the audio media. Default:
                        /home/pi/Audio/
  -w WAIT, --wait WAIT  Time to wait between button presses, must be an
                        integer Default: 0
  -g GPIO [GPIO ...], --gpio GPIO [GPIO ...]
                        GPIO Pins to use for buttons. e.g. -g 18 23 24 25
  -d DEBOUNCE, --debounce DEBOUNCE
                        Debounce for switches (in ms). If you get repeated
                        sounds increase. Default: 100ms
```

The button assignment is automatic based on the order of the video files.
Videos should be named alphabetically for best results.

```
VIDEO_1.mp4
VIDEO_2.mp4
etc....
```

The screensaver video, i.e. the video loop when no video has been selected must
be called screensaver.mp4.

The defaults are as follows:

```
media_folder = /home/pi/Video
debounce = 100ms
wait between button presses = 0 secs
```
When assigning the GPIO inputs for your buttons the order that they are put
on the command line is the order that the videos will be assigned, therefore:

```
sudo python videoplayer.py -g 18 23 24 25 -f /media/USB0 -w 2 -d 200
```

works as follows...

```
-g 12 23 24 25 --> Assign pins 18, 23, 24, 25 as your button inputs
                   (add more for more buttons). This setting is REQUIRED and
                   must be in the order that you want the videos to appear
                   18 = Video1,
                   23 = Video2,
                   24 = Video3,
                   25 = Video4,
                   etc....

-f /media/USB0 --> Use /media/USB0 as your video source directory
                   change to wherever you store your videos

-w 2 --> Wait 2 seconds between button presses

-d 200 --> Debounce with a time of 200 milliseconds
```

Check the logfile for all errors.

# Automatically Mount USB Stick

This is the simplest, and in my humble opinion the best method for mounting a USB stick (or drive) on the Raspberry Pi. This is only needed if your USB sticks are not already automatically mounting.

Run the following command and reboot - your USB Sticks will show up under the folder /media/ and the folder will be called USB - or USB2, or USB3, or USB4 depending on the order they were attached.

```
sudo apt-get install usbmount
``` 
  
This is useful for other projects such as a web kiosk where you can store all the files on the USB stick and save the SD card some read/write cycles.

## Permissions

In order to have read/write access to the files on the USB stick edit /etc/usbmount/usbmount.conf

```
sudo nano /etc/usbmount/usbmount.conf
```

There is a line in the file that looks like this:

```
FS_MOUNTOPTIONS=""
```

Change to the following. This should allow anyone to read or write the files. This does not work for Mac formatted disks.

```
FS_MOUNTOPTIONS="-fstype=vfat,gid=users,uid=nobody,umask=000,sync -fstype=ntfs,gid=users,uid=nobody,umask=000,sync"
```

# Autostarting any script

Use this method to automatically start and keep any script running

## Template

Change the bits that need changing to suit your setup

```
[Unit]
Description=Simplfied Service
After=syslog.target

[Service]
Type=simple
User=root
Group=root
WorkingDirectory=/home/pi
ExecStart=/usr/bin/python3 /home/pi/script.py
StandardOutput=syslog
StandardError=syslog

[Install]
WantedBy=multi-user.target
```

Save this file as MY-SERVICE-NAME.service and copy it to....

```
sudo cp MY-SERVICE-NAME.service /lib/systemd/system/
```

Run these commands to enable and run the service at startup

```
sudo systemctl daemon-reload
sudo systemctl enable MY-SERVICE-NAME.service
sudo systemctl start MY-SERVICE-NAME.service
```

To see it it all worked and is running (if not you're on your own)

```
sudo systemctl status MY-SERVICE-NAME.service
```