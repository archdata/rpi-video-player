#!/usr/bin/env python

"""
RPi Audio Player

# Setup

There are two options for setting up the scripts

1) settings.conf

Change the details in settings.conf to match your setup.
 Having a settings.conf precludes the use of the command line interface

2) Command line

If there is not a settings.conf or it cannot be read the script falls
back to command line mode.

optional arguments:
  -h, --help            show this help message and exit
  -f MEDIA_FOLDER, --media_folder MEDIA_FOLDER
                        Folder containing the audio media. Default:
                        /home/pi/Videos/
  -w WAIT, --wait WAIT  Time to wait between button presses, must be an
                        integer Default: 0
  -g GPIO [GPIO ...], --gpio GPIO [GPIO ...]
                        GPIO Pins to use for buttons. e.g. -g 18 23 24 25
  -d DEBOUNCE, --debounce DEBOUNCE
                        Debounce for switches (in ms). If you get repeated
                        plays increase. Default: 100ms
  -r PULL_UP_DOWN, --pull_up_down PULL_UP_DOWN
                        Use "up", "down", or "off"
Therefore,

sudo python videoplayer.py -g 18 23 24 25 -f /media/USB0 -w 2 -d 200

works as follows...

-g 18 23 24 25 --> Assign pins 18, 23, 24, 25 as your button inputs
                   (add more for more buttons). This setting is REQUIRED and
                   must be in the order that you want the videos to appear.
                   videos appear in alphabetical/numerical order
                   18 = Video1,
                   23 = Video2,
                   24 = Video3,
                   25 = Video4,
                   etc....

-f /media/USB0 --> Use /media/USB0 as your video source directory
                   change to wherever you store your videos

-w 2 --> Wait 2 seconds between button presses

-d 200 --> Debounce with a time of 200 milliseconds

-r down --> set the internal resistor to pull down the input

# Using the Script

The button assignment is automatic based on the order of the video files.
Videos should be named alphabetically for best results.

VIDEO_1.mp4
VIDEO_2.mp4
etc....

The screensaver video, i.e. the video loop when no video has been selected must
be called screensaver.mp4.

The defaults are as follows:

media_folder = /home/pi/Videos
debounce = 100ms
wait between button presses = 0 secs
pull_up_down = up

Check the log file for all errors.

"""
import os
import re
import sys
import glob
import RPi.GPIO as GPIO
import time
import signal
import argparse
import logging
import subprocess
import ConfigParser
# If the amp is used this will load the library for the Adafruit_MAX9744
try:
    from Adafruit_MAX9744 import MAX9744
except:
    pass

__author__ = "Andrew Macdonald"
__credits__ = ["Andrew Macdonald"]
__maintainer__ = "Andrew Macdonald"
__email__ = "andrew@scriptoria.ca"


class VideoPlayer(object):
    """ Raspberry Pi Video Player
    """

    video_player = 'omxplayer'
    video_player_options = '--no-keys -b -o both --vol 100'
    video_types = ['*.mp4', '*.h264', '*.avi', '*.mov', '*.mkv', '*.m4v']

    def __init__(self, args):

        self.wait_period = args['wait']
        # add trailing slash if required
        self.media_folder = os.path.join(args['media_folder'], '')
        self.gpio_pins = args['gpio']

        self.playable_files = list()
        self.current_time = time.time()
        self.button_lockout = False

        self.screensaver_video = os.path.normpath(args['screensaver'])

        # Search for the screensaver file. Set as current video
        for root, dirs, files in os.walk(self.media_folder):
            for name in files:
                if os.path.splitext(name)[0] == os.path.splitext(self.screensaver_video)[0]:
                    if ''.join(['*', os.path.splitext(name)[1]]) in VideoPlayer.video_types:
                        # Set the current video file to the screensaver file
                        self.screensaver_video = os.path.join(root, name)
                        self.current_video = self.screensaver_video
                    else:
                        logging.warning("No valid screensaver file")
                        sys.exit()

    @staticmethod
    def sorted_nicely(list_to_sort):
        """ Sorts the given iterable in the way that is expected.
        arcpy.wordpress.com/2012/05/11/sorting-alphanumeric-strings-in-python/
        """
        convert = lambda text: int(text) if text.isdigit() else text
        alphanum = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
        return sorted(list_to_sort, key=alphanum)

    @staticmethod
    def kill_omxplayer():
        """ Kill the omxplayer process to make way for a new omxplayer process
        """

        process = subprocess.Popen(
            ['ps', '-A'],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.STDOUT)

        out, _ = process.communicate()

        for line in out.splitlines():
            if 'omxplayer' in line:
                pid = int(line.split(None, 1)[0])
                os.kill(pid, signal.SIGKILL)

    def get_file_list(self):
        """ Get a list of video files from media_folder
        """

        logging.info("Finding and prepping all playable video files")

        video_files = list()
        for video_name in VideoPlayer.video_types:
            video_files.extend(glob.glob(os.path.join(
                self.media_folder,
                video_name)))

        if video_files:
            try:
                video_files.remove(self.screensaver_video)
            except ValueError:

                logging.warning('No screensaver file in media folder.'
                                'Please add a screensaver to continue')
                sys.exit()

            video_files = VideoPlayer.sorted_nicely(video_files)

            for files in video_files:
                self.playable_files.append(files)
        else:
            logging.warning('No video files found in media folder')
            sys.exit()

    def button_press_callback(self, pin):
        """ Interrupt Callback. Take the GPIO button presses and do something
            Checks to see if the pin is in the list of GPIO pins being used.
            It always should be but checking just in case.
        """

        if pin in self.gpio_pins:

            if GPIO.input(pin) == True:

                wait_timer = float("%.2f" % (time.time() - self.current_time))

                if wait_timer >= self.wait_period:
                    self.current_time = time.time()
                    self.button_lockout = False

                if self.button_lockout is False:
                    self.button_lockout = True

                    for index, value in enumerate(self.gpio_pins):
                        if pin == value:
                            try:
                                logging.info("Play selected video")
                                self.kill_omxplayer()
                                self.current_video = self.playable_files[index]
                            except IndexError:
                                logging.warning("No video file for button "
                                                "connected to pin %s ", pin)

        else:
            logging.warning("The GPIO pins being used is not"
                            " in the list of Pins you setup")

    def play_current_video(self):
        """ Create the play command all filled in with the file
        to play and location
        """

        command = " ".join([
            VideoPlayer.video_player,
            VideoPlayer.video_player_options,
            self.current_video])

        try:
            subprocess.check_call(
                command,
                shell=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)

        except subprocess.CalledProcessError as error:
            logging.info(error.returncode)
        else:
            self.current_video = self.screensaver_video


def parse_args(defaults):
    """ Parse the command line arguments
    """

    parser = argparse.ArgumentParser(description='RPi Audio Player')

    parser.add_argument(
        '-f',
        '--media_folder',
        action="store",
        default=defaults['media_folder'],
        help="Folder containing the audio media. Default: /home/pi/Videos/")

    parser.add_argument(
        '-w',
        '--wait',
        action="store",
        default=defaults['wait'],
        type=int,
        help='Time to wait between presses, must be an integer Default: 0')

    parser.add_argument(
        '-g',
        '--gpio',
        nargs='+',
        default=None,
        type=int,
        required=True,
        help='GPIO Pins to use for buttons. e.g. -g 18 23 24 25')

    parser.add_argument(
        '-d',
        '--debounce',
        action="store",
        default=defaults['debounce'],
        type=int,
        help='Debounce for switches (in ms). If you get repeated sounds increase. Default: 100ms')

    parser.add_argument(
        '-r',
        '--pull_up_down',
        action="store",
        default=defaults['pull_up_down'],
        help='Use "up", "down", or "off"'
    )

    parser.add_argument(
        '-s',
        '--screensaver_file',
        action="store",
        default=defaults['screensaver_file'],
        help='Name of screensaver file - works with or without the extension'
    )

    args = vars(parser.parse_args())
    return args


def config_section_map(section):
    """ Helper function for config file settings
    """
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                logging.warning("skip: %s", option)
        except Exception as error:
            logging.warning(error)
            logging.warning("exception on %s!", option)
            dict1[option] = None
    return dict1


def hide_command_line():
    """ This sets the text colour of the command line to black
    to hide any screen output
    NOTE: this will hide all text output on the command line
     - exit and log back in to get it back or type 'reset'.
    """
    hide_text_command = 'sudo sh -c\
        "TERM=linux setterm -foreground black -clear all > /dev/tty0"'
    subprocess.Popen(
        hide_text_command,
        shell=True,
        stdout=subprocess.PIPE)


def show_command_line():
    """ This sets the text colour of the command line to white
    """
    show_text_command = 'sudo sh -c\
        "TERM=linux setterm -foreground white -clear all > /dev/tty0"'
    subprocess.Popen(
        show_text_command,
        shell=True,
        stdout=subprocess.PIPE)


def main():
    """ Main Function
    """

    hide_command_line()

    # Defaults settings
    defaults = dict()
    defaults['media_folder'] = '/home/pi/Videos/'
    defaults['wait'] = 0
    defaults['debounce'] = 100
    defaults['pull_up_down'] = 'up'
    defaults['screensaver'] = 'screensaver'

    logging.info("Parsing command line arguments")

    try:
        config_file = ''.join([os.path.dirname(os.path.realpath(__file__)), '/settings.conf'])
        Config.readfp(open(config_file))

        defaults['media_folder'] = os.path.join(config_section_map("MediaFolder")['media_folder'], '')
        defaults['wait'] = int(config_section_map("WaitTimer")['wait'])
        defaults['debounce'] = int(config_section_map("Debounce")['debounce'])
        defaults['gpio'] = config_section_map("GPIOPins")['pins']
        defaults['pull_up_down'] = config_section_map("GPIOPins")['pull_up_down']
        defaults['screensaver'] = config_section_map("Screensaver")['screensaver_file']

        if defaults['pull_up_down'].lower() not in ['up', 'down', 'off']:
            logging.warning('Not valid internal resistor setting: Use "up", "down" or "off"')
            sys.exit()

        if defaults['gpio']:
            defaults['gpio'] = map(int, defaults['gpio'].split(','))
        else:
            logging.warning('No GPIO pins have been defined in settings.conf')
            sys.exit()

        args = defaults

    except IOError:
        logging.warning('No settings.conf file. Falling back to command line'
                        'options')
        args = parse_args(defaults)
    except KeyError as e:
        logging.warning(''.join(['settings.conf error on line: ', str(e)]))
        sys.exit()

    player = VideoPlayer(args)

    player.get_file_list()

    # This is set to BCM - so use GPIO numbers, not pin numbering
    GPIO.setmode(GPIO.BCM)
    # Setup the amp volume if used otherwise don't worry about it
    try:
        amp = MAX9744()
        amp.set_volume(50)
    except:
        pass

    for pin in args['gpio']:
        try:
            if args['pull_up_down'].lower() == 'up':
                 GPIO.setup(pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
            elif args['pull_up_down'].lower() == 'down':
                 GPIO.setup(pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
            elif args['pull_up_down'].lower() == 'off':
                GPIO.setup(pin, GPIO.IN)
        except Exception as e:
            logging.warning(e)

        GPIO.add_event_detect(
            pin,
            GPIO.BOTH,
            callback=player.button_press_callback,
            bouncetime=args['debounce'])

    while True:
        try:
            player.play_current_video()
        except KeyboardInterrupt:
            show_command_line()
            sys.exit()


if __name__ == '__main__':

    LOG_LEVEL = logging.CRITICAL
    LOG_FILENAME = os.path.splitext(os.path.basename(__file__))[0] + '.log'
    logging.basicConfig(
        filename=LOG_FILENAME,
        filemode='w',
        format='%(asctime)s | %(levelname)s | %(lineno)d | %(message)s',
        level=LOG_LEVEL)

    Config = ConfigParser.ConfigParser()

    main()
